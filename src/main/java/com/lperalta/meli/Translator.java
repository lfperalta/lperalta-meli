package com.lperalta.meli;

public class Translator {

	Parametrization parametrization;
	StringBuffer sbResult;
	StringBuffer sbResultHuman;
	int indexFullStop;

	public Translator() {
		this.parametrization = new Parametrization();
		this.sbResult = new StringBuffer();
		this.sbResultHuman = new StringBuffer();
		this.indexFullStop = 0;
	}

	/**
	 * 
	 * @param text:
	 *            texto que contendr� un literal de bits enviados y que ser�n
	 *            decodificados
	 * @return: retorna un string correpondiente al c�digo morse que representa
	 *          la secuencia de bits
	 * @throws Exception
	 */
	public String decodeBits2Morse(String text) throws Exception {
		text = text.replaceAll("\\s+","");
		
		int count = 0;
		for (int i = 0; i < text.length(); i++) {
			byte c = (byte) Character.getNumericValue(text.charAt(i));

			if (i == 0) {
				count++;

			} else {
				byte cAnt = (byte) Character.getNumericValue(text.charAt(i - 1));

				if (c == cAnt && !(text.length() - 1 == i)) {
					count++;
				} else if (text.length() - 1 == i) {
					count++;

					parametrization(count, cAnt, this.parametrization);
				} else {

					parametrization(count, cAnt, this.parametrization);
					count = 1;
				}
			}
		}
		
		count = 0;
		for (int i = 0; i < text.length(); i++) {
			byte c = (byte) Character.getNumericValue(text.charAt(i));

			if (i == 0) {
				count++;

			} else {
				byte cAnt = (byte) Character.getNumericValue(text.charAt(i - 1));

				if (c == cAnt && !(text.length() - 1 == i)) {
					count++;
				} else if (text.length() - 1 == i) {
					count++;
					calculate(count, cAnt, sbResult, parametrization);
				} else {
					calculate(count, cAnt, sbResult, parametrization);
					count = 1;
				}
			}
		}

		this.indexFullStop = getFullStop(sbResult.toString().trim());
		if (indexFullStop > 0)
			return sbResult.toString().trim().substring(0, indexFullStop).trim();
			
		return sbResult.toString().trim();

	}

	/**
	 * M�todo que convierte c�digo morse a texto
	 * 
	 * @param text:
	 *            C�digo a ser traducido.
	 * @return: Un string que puede ser le�do por los humanos.
	 */
	public String translate2Human(String text) {
		this.indexFullStop = getFullStop(text.trim());
		if (indexFullStop > 0)
			text = text.trim().substring(0, indexFullStop).trim();
		
		String[] separatorWords = text.split("      ");		

		for (String sp : separatorWords) {
			String[] words = sp.split(" ");
			
			for (String string : words) {
				this.sbResultHuman.append(morse2Human(string));
			}
			
			this.sbResultHuman.append(" ");
		}

		return this.sbResultHuman.toString().trim();
	}
	
	/**
	 * M�todo que convierte texto en c�digo morse
	 * 
	 * @param text:
	 *            texto entendido por humanos.
	 * @return: String que representa c�digo morse del texto ingresado.
	 */
	public String textToMorse(String text) {
		for (char ch: text.toUpperCase().toCharArray()) {
			this.sbResultHuman.append(String.valueOf(human2Morse(ch)));
		}

		return this.sbResultHuman.toString().trim();
	}
	
	/**
	 * M�todo privado que se utiliza para asociar letras y n�meros del alfabeto
	 * con su representaci�n en morse,
	 * 
	 * @param morseCode:
	 *            c�digo morse que ser� traducido a lenguaje Humano
	 * @return retorna el caracter correspondiente al c�digo ingresado.
	 */
	private String morse2Human(String morseCode) {
		String result;
		switch (morseCode) {
		case AlphabetMorse.LETTER_A:
			result = "A";
			break;
		case AlphabetMorse.LETTER_B:
			result = "B";
			break;
		case AlphabetMorse.LETTER_C:
			result = "C";
			break;
		case AlphabetMorse.LETTER_D:
			result = "D";
			break;
		case AlphabetMorse.LETTER_E:
			result = "E";
			break;
		case AlphabetMorse.LETTER_F:
			result = "F";
			break;
		case AlphabetMorse.LETTER_G:
			result = "G";
			break;
		case AlphabetMorse.LETTER_H:
			result = "H";
			break;
		case AlphabetMorse.LETTER_I:
			result = "I";
			break;
		case AlphabetMorse.LETTER_J:
			result = "J";
			break;
		case AlphabetMorse.LETTER_K:
			result = "K";
			break;
		case AlphabetMorse.LETTER_L:
			result = "L";
			break;
		case AlphabetMorse.LETTER_M:
			result = "M";
			break;
		case AlphabetMorse.LETTER_N:
			result = "N";
			break;
		case AlphabetMorse.LETTER_O:
			result = "O";
			break;
		case AlphabetMorse.LETTER_P:
			result = "P";
			break;
		case AlphabetMorse.LETTER_Q:
			result = "Q";
			break;
		case AlphabetMorse.LETTER_R:
			result = "R";
			break;
		case AlphabetMorse.LETTER_S:
			result = "S";
			break;
		case AlphabetMorse.LETTER_T:
			result = "T";
			break;
		case AlphabetMorse.LETTER_U:
			result = "U";
			break;
		case AlphabetMorse.LETTER_V:
			result = "V";
			break;
		case AlphabetMorse.LETTER_W:
			result = "W";
			break;
		case AlphabetMorse.LETTER_X:
			result = "X";
			break;
		case AlphabetMorse.LETTER_Y:
			result = "Y";
			break;
		case AlphabetMorse.LETTER_Z:
			result = "Z";
			break;
		case AlphabetMorse.POINT:
			result = ".";
			break;
		case AlphabetMorse.NUMBER_0:
			result = "0";
			break;
		case AlphabetMorse.NUMBER_1:
			result = "1";
			break;
		case AlphabetMorse.NUMBER_2:
			result = "2";
			break;
		case AlphabetMorse.NUMBER_3:
			result = "3";
			break;
		case AlphabetMorse.NUMBER_4:
			result = "4";
			break;
		case AlphabetMorse.NUMBER_5:
			result = "5";
			break;
		case AlphabetMorse.NUMBER_6:
			result = "6";
			break;
		case AlphabetMorse.NUMBER_7:
			result = "7";
			break;
		case AlphabetMorse.NUMBER_8:
			result = "8";
			break;
		case AlphabetMorse.NUMBER_9:
			result = "9";
			break;
		default:
			result = "";
		}
		return result;
	}
	
	private String human2Morse(char humanString) {
		String result;
		switch (humanString) {
		case 'A':
			result = AlphabetMorse.LETTER_A + AlphabetMorse.SPACE;
			break;
		case 'B':
			result = AlphabetMorse.LETTER_B + AlphabetMorse.SPACE;
			break;
		case 'C':
			result = AlphabetMorse.LETTER_C + AlphabetMorse.SPACE;
			break;
		case 'D':
			result = AlphabetMorse.LETTER_D + AlphabetMorse.SPACE;
			break;
		case 'E':
			result = AlphabetMorse.LETTER_E + AlphabetMorse.SPACE;
			break;
		case 'F':
			result = AlphabetMorse.LETTER_F + AlphabetMorse.SPACE;
			break;
		case 'G':
			result = AlphabetMorse.LETTER_G + AlphabetMorse.SPACE;
			break;
		case 'H':
			result = AlphabetMorse.LETTER_H + AlphabetMorse.SPACE;
			break;
		case 'I':
			result = AlphabetMorse.LETTER_I + AlphabetMorse.SPACE;
			break;
		case 'J':
			result = AlphabetMorse.LETTER_J + AlphabetMorse.SPACE;
			break;
		case 'K':
			result = AlphabetMorse.LETTER_K + AlphabetMorse.SPACE;
			break;
		case 'L':
			result = AlphabetMorse.LETTER_L + AlphabetMorse.SPACE;
			break;
		case 'M':
			result = AlphabetMorse.LETTER_M + AlphabetMorse.SPACE;
			break;
		case 'N':
			result = AlphabetMorse.LETTER_N + AlphabetMorse.SPACE;
			break;
		case 'O':
			result = AlphabetMorse.LETTER_O + AlphabetMorse.SPACE;
			break;
		case 'P':
			result = AlphabetMorse.LETTER_P + AlphabetMorse.SPACE;
			break;
		case 'Q':
			result = AlphabetMorse.LETTER_Q + AlphabetMorse.SPACE;
			break;
		case 'R':
			result = AlphabetMorse.LETTER_R + AlphabetMorse.SPACE;
			break;
		case 'S':
			result = AlphabetMorse.LETTER_S + AlphabetMorse.SPACE;
			break;
		case 'T':
			result = AlphabetMorse.LETTER_T + AlphabetMorse.SPACE;
			break;
		case 'U':
			result = AlphabetMorse.LETTER_U + AlphabetMorse.SPACE;
			break;
		case 'V':
			result = AlphabetMorse.LETTER_V + AlphabetMorse.SPACE;
			break;
		case 'W':
			result = AlphabetMorse.LETTER_W + AlphabetMorse.SPACE;
			break;
		case 'X':
			result = AlphabetMorse.LETTER_X + AlphabetMorse.SPACE;
			break;
		case 'Y':
			result = AlphabetMorse.LETTER_Y + AlphabetMorse.SPACE;
			break;
		case 'Z':
			result = AlphabetMorse.LETTER_Z + AlphabetMorse.SPACE;
			break;
		case '.':
			result = AlphabetMorse.POINT + AlphabetMorse.SPACE;
			break;
		case '0':
			result = AlphabetMorse.NUMBER_0 + AlphabetMorse.SPACE;
			break;
		case '1':
			result = AlphabetMorse.NUMBER_1 + AlphabetMorse.SPACE;
			break;
		case '2':
			result = AlphabetMorse.NUMBER_2 + AlphabetMorse.SPACE;
			break;
		case '3':
			result = AlphabetMorse.NUMBER_3 + AlphabetMorse.SPACE;
			break;
		case '4':
			result = AlphabetMorse.NUMBER_4 + AlphabetMorse.SPACE;
			break;
		case '5':
			result = AlphabetMorse.NUMBER_5 + AlphabetMorse.SPACE;
			break;
		case '6':
			result = AlphabetMorse.NUMBER_6 + AlphabetMorse.SPACE;
			break;
		case '7':
			result = AlphabetMorse.NUMBER_7 + AlphabetMorse.SPACE;
			break;
		case '8':
			result = AlphabetMorse.NUMBER_8 + AlphabetMorse.SPACE;
			break;
		case '9':
			result = AlphabetMorse.NUMBER_9 + AlphabetMorse.SPACE;
			break;
		case ' ':
			result = AlphabetMorse.SEPARATE_WORD + AlphabetMorse.SPACE;
			break;			
		default:
			throw new IllegalArgumentException("c�digo inv�lido: " + humanString);
		}
		return result;
	}

	/**
	 * M�todo privado que analiza la secuencia de bits enviados y que coninciden
	 * con las correspondientes pausas, tiempos de espera.
	 * @param count Cantidad de posiciones que ocupa el par�metro c
	 * @param c Respresenta el bit 1 un pulso y el 0 una pausa
	 * @param sb Atributo de la clase que almacenar� la parametrizaci�n.
	 * @param p Atributo que contendr� la configuraci�n para determinar las secciones del mensaje
	 */
	private void calculate(int count, Byte c, StringBuffer sb, Parametrization p) {
		if (count >= p.getPoint() && count <= standardDeviation(p.getPoint(),p.getLine())  && c == 1) {
			sb.append(".");
		} else if (count <= p.getLine() && count > standardDeviation(p.getPoint(),p.getLine())  && c == 1) {
			sb.append("-");
		} else if (count >= p.getSeparateLetter() && count <= standardDeviation(p.getSeparateLetter(),p.getSeparateWord()) && c == 0) {
			sb.append("");
		} else if (((count > standardDeviation(p.getSeparateLetter(),p.getSeparateWord()) && 
				count <= (p.getSeparateWord()-standardDeviation(p.getSeparateLetter(),p.getSeparateWord()))) || 
				count >= p.getSeparateWord())  && c == 0) {
			sb.append(" ");
		} else {
			sb.append("      ");
		}
	}
	
	/**
	 * * M�todo privado que parametriza las longitudes de referencia de las distintas 
	 * secciones del mensaje
	 * @param count Cantidad de posiciones que ocupa el par�metro c
	 * @param c Respresenta el bit 1 un pulso y el 0 una pausa
	 * @param p Atributo de la clase que almacenar� la parametrizaci�n.
	 */
	private void parametrization(int count, Byte c, Parametrization p){

		if(c == 0 && p.getFinish() < count){
		      p.setFinish(count);
		      p.setSeparateWord(p.getFinish()-1);
		}
		
		if(c == 0 && p.getSeparateLetter() == 0){
			p.setSeparateLetter(count);
		}else if(c == 0 && p.getSeparateLetter() > count){
		      p.setSeparateLetter(count);
		}
		
		if(c == 1 && p.getLine() == 0){
			p.setLine(count);
		}
		else if(c == 1 && p.getLine() < count){
		      p.setLine(count);
		}
		
		if(c == 1 && p.getPoint() == 0){
			p.setPoint(count);
		}else if(c == 1 && p.getPoint() > count){
		      p.setPoint(count);
		}

	}

	
	/**
	 * 
	 * @param msg: mensaje en c�digo morse para chequear si tiene fin del mensaje
	 * @return retorna el indice del FULL_STOP
	 */
	private int getFullStop(String msg){
		return msg.lastIndexOf(AlphabetMorse.FULL_STOP);
	}
	
	
	/**
	 * M�todo privado que determina la desviaci�n estandar para calcular los intervalos de las distinas 
	 * secciones del mensaje
	 * @param p1 parametro del que se quiere obtener la variaci�n estandar en realaci�n a p2
	 * @param p2 parametro del que se quiere obtener la variaci�n estandar en realaci�n a p1
	 * @return retorna un entero con la desviaci�n estandar correpondiente
	 */
	private int standardDeviation(int p1, int p2) {
		  int sumatoria = 0;
		  float media = 0;
		  double varianza = 0.0;
		  double desviacion= 0.0; 
		   
		   
		  sumatoria = p1 + p2;
		  
		  media = sumatoria / 10; //media aritmetica 
		  

		   double rango;
		   rango = Math.pow(p1 - media, 2f);
		   varianza = varianza + rango;
		   
		   rango = Math.pow(p2 - media, 2f);
		   varianza = varianza + rango;
		  
		  varianza = varianza / 10f;
		  
		  //desviaci�n estandar
		  desviacion = Math.sqrt(varianza);
		  
		  //retorno redondeando para arriba
		  return (int) Math.ceil(desviacion);
		 }

}