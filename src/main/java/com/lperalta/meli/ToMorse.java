package com.lperalta.meli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class MorseAppEngine
 */
@WebServlet(name = "Morse", urlPatterns = { "/translate/2morse" })
public class ToMorse extends HttpServlet implements BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ToMorse() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		try {

			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");

			Translator translator = new Translator();

			String text = request.getParameter("text");
			String bits = request.getParameter("bits");
			if (text != null) {
				String result = translator.textToMorse(text);
				ServletUtils.responseResultGet(response, result);

			} else if (bits != null) {
				String result = translator.decodeBits2Morse(bits);
				ServletUtils.responseResultGet(response, result);
			}else{    
				response.setStatus(400);
				ServletUtils.responseResultGet(response, null);
	    	}

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(400);
			ServletUtils.responseResultGet(response, null);
		}
	}

	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	
    	Translator translate = new Translator();
    	PrintWriter out = response.getWriter();
    	
		try {
			String text = request.getParameter("text");
			String bits = request.getParameter("bits");
			
	    	if(text != null){
				String result = translate.textToMorse(text);
				ServletUtils.responseResultPost(response, result, out);
			} else if (bits != null) {
				String result = translate.decodeBits2Morse(bits);
				ServletUtils.responseResultPost(response, result, out);  
	    	} else {
	    		goJsonPost(translate, request, out, response);
	    	}
			
	    	out.close();

		} catch (Exception e) {
			response.setStatus(400);
			ServletUtils.responseResultPost(response, null, out); 
		}
	}
	
	public void goJsonPost(Translator translate, HttpServletRequest request, PrintWriter out, HttpServletResponse response) throws Exception{
    	StringBuffer jb = new StringBuffer();
    	String line = null;

    	BufferedReader reader = request.getReader();
    	while ((line = reader.readLine()) != null)
    		jb.append(line);

    	JsonParser parser = new JsonParser();
    	JsonElement json = parser.parse(jb.toString());
    	JsonObject object=json.getAsJsonObject();
    	String jsonTextParamText = object.get("text") != null ? object.get("text").toString() : null;
    	String jsonTextParamBits = object.get("bits") != null ? object.get("bits").toString() : null;

    	if(jsonTextParamText != null){
    		jsonTextParamText = jsonTextParamText.replaceAll("\"","");
    		String result = translate.textToMorse(jsonTextParamText);

    		out = response.getWriter();
    		ServletUtils.responseResultPost(response, result, out);  	
    	}else if(jsonTextParamBits != null){
    		jsonTextParamBits = jsonTextParamBits.replaceAll("\"","");
    		String result = translate.decodeBits2Morse(jsonTextParamBits);

    		out = response.getWriter();
    		ServletUtils.responseResultPost(response, result, out);  	
    	}else{    
    		response.setStatus(400);
    		ServletUtils.responseResultPost(response, null, out); 
    	}
    }

}
