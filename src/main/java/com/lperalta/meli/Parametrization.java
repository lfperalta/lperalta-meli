package com.lperalta.meli;

public class Parametrization {
	private int point = 0;
	private int line = 0;
	private int separateLetter = 0;
	private int separateWord = 0;
	private int finish = 0;
	
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public int getSeparateLetter() {
		return separateLetter;
	}
	public void setSeparateLetter(int separateLetter) {
		this.separateLetter = separateLetter;
	}
	public int getSeparateWord() {
		return separateWord;
	}
	public void setSeparateWord(int separateWord) {
		this.separateWord = separateWord;
	}
	public int getFinish() {
		return finish;
	}
	public void setFinish(int finish) {
		this.finish = finish;
	}
	
	
}
