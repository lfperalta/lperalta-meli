package com.lperalta.meli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class MorseAppEngine
 */
@WebServlet(
    name = "Text",
    urlPatterns = {"/translate/2text"}
)
public class ToText extends HttpServlet implements BaseServlet{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToText() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
    		throws IOException {

    	response.setContentType("text/plain");
    	response.setCharacterEncoding("UTF-8");

    	String text = request.getParameter("text");

    	if(text != null){
    		Translator translate = new Translator();
    		String result = translate.translate2Human(text);

    		ServletUtils.responseResultGet(response, result);
    	}else{    
    		response.setStatus(400);
    		ServletUtils.responseResultGet(response, null);
    	}
    }
    
    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    		throws IOException {

    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	
    	Translator translate = new Translator();
    	PrintWriter out = response.getWriter();
    	
    	String text = request.getParameter("text");

    	if(text != null){
    		String result = translate.translate2Human(text);

    		out = response.getWriter();
    		ServletUtils.responseResultPost(response, result, out); 
    	} else {
    		goJsonPost(translate, request, out, response);
    	}

    	out.close();
    }
    
    public void goJsonPost(Translator translate, HttpServletRequest request, PrintWriter out, HttpServletResponse response) throws IOException{
    	StringBuffer jb = new StringBuffer();
    	String line = null;

    	BufferedReader reader = request.getReader();
    	while ((line = reader.readLine()) != null)
    		jb.append(line);

    	JsonParser parser = new JsonParser();
    	JsonElement json = parser.parse(jb.toString());
    	JsonObject object=json.getAsJsonObject();
    	String jsonTextParam = object.get("text") != null ? object.get("text").toString() : null;

    	if(jsonTextParam != null){
    		jsonTextParam = jsonTextParam.replaceAll("\"","");
    		String result = translate.translate2Human(jsonTextParam);

    		out = response.getWriter();
    		ServletUtils.responseResultPost(response, result, out);  	
    	}else{    
    		response.setStatus(400);
    		ServletUtils.responseResultPost(response, null, out); 
    	}
    }

}
