package com.lperalta.meli;

public class AlphabetMorse {
	public static final String LETTER_A = ".-";
	public static final String LETTER_B = "-...";
	public static final String LETTER_C = "-.-.";
	public static final String LETTER_D = "-..";
	public static final String LETTER_E = ".";
	public static final String LETTER_F = "..-.";
	public static final String LETTER_G = "--.";
	public static final String LETTER_H = "....";
	public static final String LETTER_I = "..";
	public static final String LETTER_J = ".---";
	public static final String LETTER_K = "-.-";
	public static final String LETTER_L = ".-..";
	public static final String LETTER_M = "--";
	public static final String LETTER_N = "-.";
	public static final String LETTER_O = "---";
	public static final String LETTER_P = ".--.";
	public static final String LETTER_Q = "--.-";
	public static final String LETTER_R = ".-.";
	public static final String LETTER_S = "...";
	public static final String LETTER_T = "-";
	public static final String LETTER_U = "..-";
	public static final String LETTER_V = "...-";
	public static final String LETTER_W = ".--";
	public static final String LETTER_X = "-..-";
	public static final String LETTER_Y = "-.--";
	public static final String LETTER_Z = "--..";
	public static final String POINT = ".-.-.-";
	public static final String NUMBER_0 = "-----";
	public static final String NUMBER_1 = ".----";
	public static final String NUMBER_2 = "..---";
	public static final String NUMBER_3 = "...--";
	public static final String NUMBER_4 = "....-";
	public static final String NUMBER_5 = ".....";
	public static final String NUMBER_6 = "-....";
	public static final String NUMBER_7 = "--...";
	public static final String NUMBER_8 = "---..";
	public static final String NUMBER_9 = "----.";
	public static final String SPACE = " ";
	public static final String SEPARATE_WORD = "     ";
	public static final String FULL_STOP = ".-.-.-";
	
}
