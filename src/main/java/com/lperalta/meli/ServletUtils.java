package com.lperalta.meli;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class ServletUtils {

	public static void responseResultGet(HttpServletResponse response, String result) throws IOException {
		response.getWriter().print("{code: " + response.getStatus() + ", " + "response:'" + (result != null ? result : "bad request") + "'}");
	}
	
	public static void responseResultPost(HttpServletResponse response, String result, PrintWriter out) throws IOException {
		out.println("{code: " + response.getStatus() + ", " + "response:'" + (result != null ? result : "bad request") + "'}");
	}
	
}
