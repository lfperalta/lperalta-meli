package com.lperalta.meli;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface BaseServlet {
    public abstract void goJsonPost(Translator translate, HttpServletRequest request, PrintWriter out, HttpServletResponse response) throws IOException, Exception;
}
